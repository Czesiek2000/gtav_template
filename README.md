# Gta V C++ template 

## Repository
This repository is created for people who want to make a gta v mods with c++ but have some troubles / issues with setting up a Visual Studio Project. This template may have some bugs, but it is fairly possible. Look for the instructions, make sure you've done everything correctly, and that you follow this quick instruction.

# Downloading info
There are 2 ways of downloading and using these project. 
    1. You can do it with release page
    2. You can do it using git or git GUI aplications. 

## Graphical instruction
Make sure to check the graphical instruction that I include in these repo in `/doc` folder! 
It can help you with instalation, basic usage of these repo or with some error trouble.

## Instalation
 ! To do this you need to have git installed on your computer !
* !!! if you not familiar with git download repo with zip 
  * with git 
    1. if you want to clone the repo open command line like git bash with git installed 
    2. `cd 'Project directory'`
    3. if you don't have git go to `git-scm.com` and click 
        * zip file will download on your computer 
        * find it and open 
        * You have git installed on your computer 
        * Now, follow the [Instructions](#Instruction)
    4. you have downloaded the repo see the instruction

#### **If you download the file with git you can delete /docs (there is graphic instruction how to do things with this template) and this readme and gitignore file**


* Download a Template
    1. go [here](https://gitlab.com/Czesiek2000/gtav_template/tags) to release page
    2. Found the last release (at the top)
    3. find it on your machine (probably in downloads if you have no clue where it is go to your browser and click `Ctr+J` or with 3 dots in the top right corner and found download files. Then click show in folder / Explorator windows)
    4. copy to your VisualStudioX template folder (look down for instruction / X - visual studio version recommended 2017)
    5. Do not unpack it !
    6. Copy the .zip file to the VisualStudioX directory
    7. Open VS and check if the template is available (look down how to do this)
    8. You made it
    9. Now you can start scripting in c++ with this template :) good luck


###### Check down in this readme or in another readme file or in snippets / wiki for some code snippets
___

## Instruction
#### Visual Studio Template
You can use this template as a empty project solution or as a template in Visual Studio.
You need to download files specially you need inc and lib folders.
Solution file and source (.cpp ) files are included in template.
To use this as a template download from release zip folder. Find your visual studio template folder (it might be at `Disk:/Users/user/Documents/VisualStudioX/Templates` X - version of your visual studio). Place the zip folder that you've download (don't extract it. Visual Studio use zip file as a template).
Open Visual Studio open `File->New Project->VisualC++->Template`
Find your project directory. Right click on project solution and click 3-rd from end open with Explorator. This is your solution directory. Go one up where you have your .sln file. Copy there inc and lib folders. Open Visual Studio click `Build -> Build Solution` or click `Ctr+Shift+B`. If everything goes correctly you have full setup template and you are ready to start coding with this template

#### Just a solution directory
You need to clone the directory and check with `Ctr+Shift+B` that everything is working fine.
Look to Instalation how to get this repo on your PC.
Go to your solution folder where you make a solution and double click on `.sln` file
If not check the errors. 
If VS can't file any files make sure you include correctly or copy all files to the directory. 



## Scripting
You need to have installed Visual Studio. I used for this project version community from 2017. It is required but not necessary but it can effect some problems. 
To start open the solution project (.sln file)
Inside the project you'll find some files (.cpp - source files and .h - headers files). In the utils.cpp and keyboard.cpp you don't do anything (it is used for keyboard click or for ScriptHook to work correctly). Main.cpp file is necessary to reload the .asi file (compiled .cpp extension in GTA V) in game when developing without exiting the game. I will also comment some files in future(some are comment but less). Main.h is header file that main.cpp required to run correctly. 
There is also script.cpp (there you will be scripting) and script.h. Script.h file is scriptMain which is necessary to compile the script.cpp and also some other .h files to work everything fine.
In script.cpp you have some code: 
```cpp
void main()
{
	// initialize code
	while (true)
	{
		// code that need a loop
		WAIT(0);
	}
}
```
Here goes all you script. If you need while loop you need to place it there and don't forget WAIT(0) it is required with ScriptHook to make correctly while loop

and : 
```cpp
void ScriptMain()
{
	srand(GetTickCount());
	main();
}
```

That is function that is used to load your script in game (! If don't know what to do here don't touch it. )

## Description
It took me realy long to make this project because I'm not a master of c++ coding, so it can have some bugs or defects. I found lot of template for c# code but I have've found any repos for c++. I also tryed to include some code that will help begginer to start with some useful code and links. If you know something please post some useful code snippets in these repo. 

## Licence 
MIT 
This licence is under MIT licence.
Feel free to change and improve that repository. If you found some bugs post a issue in this repo. 

