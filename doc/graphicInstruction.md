# This is image Instruction for my GTA V coding template 

## Folder & File structure
![9](/uploads/0a62d8b5b752faa596ce33b5129e7cb4/9.png)
Folder structure (You need to have the same as this)
* bin there is compiled your .asi script and folder generated with Visual Studio for debug (it will show after build successful project `Ctr+Shift+B` in VS)
  * x64 - your platform
    * Debug there is your .asi file
    * Logs there are all logs generated VisualStudio debug files
* inc all header that you need (you need to copy to the new project when you use a template)
* lib ScriptHookV.lib file important file to prompt code in Visual Studio (vs intelicence need this to prompt you some commands after clicking `Ctr+Space`)
* TestTemplate - your .vcxproj and all .cpp source files
* TestTemplate.sln - solution file (It can be named different if you use template). VisualStudio place this file the same as above folder automaticaly after creating a solution

#### Where to find your .asi file 

![11](/uploads/a28dbed86d821131c46759dc4bc08132/11.png)

After you click `Ctr+Shift+B` you will see Debugger with output logs and error list. 
If you select the path and copy to your Explorator and hit enter you will go to folder where is your .asi file
Under this Visual Studio show you if the project is Build / Failed / Nothing change in code (don't compile). 1 means everything goes correctly. 0 mens that something crash or goes not correctly. Check the steps down in this file. If it shows that is up-to-date that means that you need to make some changes in the file

## Visual studio property page 
## **! Do this if you have some problems with building the template or project**
##### This is how to get inside property page page in Visual Studio

![12](/uploads/3c9e0a2c154ee110b3bbf638bfd1ed8e/12.png)

* Right click on your project name (like on the screenshot not on the solution). 
* List of options will show 
* Left click on properties 
* The property page will pop up

# This is a first page of property page 
![1](/uploads/9b491280f9ae625fbbd5b610593b5701/1.png)

* Target name - you can change what your project is named where you compile. It can be different with this option from the name of solution project name (I haven't checked if this works so you do this this for your own risk)
* Make sure you have .asi extension selected
* Make sure you have Dynamic Library(.dll) selected
  * If you have something else type there like .exe 
    * you need to click on target extension rubric (you can type the .asi directly in this window or you can click the triangle that pop up in corner. There will popup a window and you can type there with live view of your extension)
    
![2](/uploads/68308f2142b5768f7eee2196504bf8f8/2.png)

If something is not working include there the path to the inc folder. Go to the VC++ Directories -> Include Directories
`Disk: Users\user\path to inc folder`
Just click the path the triangle will popup click it then click edit and then will popup window where you need to click the blank space there will show a line with three dots. Click the dots and search for path to the inc folder

![13](/uploads/1963520acb8e3dfab35ed76c04160fbe/13.png)

This image show how to include the path to project as I menthion above. If you have some troubles

![3](/uploads/2b0baa2f94d04fdf93caff8a0da7a212/3.png)

You need to go to to Linker -> General -> Addition Library Directories and include path to lib folder. Make it the same as the first example 
Path to your folder must be like this `Disk: Users\user\path to lib folder`. 

![4](/uploads/017d56c9a0ac0089ad45ecf5710c6f12/4.png)

If you have error like can't open ScriptHookV.obj. It means that this step has error and you typed something wrong or VS can't include .lib file that I've included.
Go to Linker -> Input -> Addition Dependencies. Click triangle, click the blank space and type there `ScriptHookV.lib` there 

![5](/uploads/6918fa1030d8cd6d58999e0030785937/5.png)

Most errors I've got was with platform compatibility. You need to have setup active solution platform the same as platform. Configuration can be Debug / Release / All Configuration (know that what you select there that will your project be named)

## Visual Studio actions
![6](/uploads/e93802ca2e08cbf951a51e4cfe1053af/6.png)


To create new project go to File -> New -> Project..

![14](/uploads/ae2ea82f5b55c520c3728d460a233db5/14.png)

Then this window will popup. If you add template to Visual Studio template folder it will show here. Type the name (it is the same as solution). Click browse to save in different than default Visual Studio place. If you save and don't remember where you've saved it after successful build solution the path will pop up. Make sure that create solution is ticked. 

![7](/uploads/83c9a59bbffce8f97299eb7e1eb6bc08/7.png)

This screenshot show you how to build solution. Just click on the Build -> Build Solution or you can press `Ctr+Shift+B`

![8](/uploads/3dbeb18387575c218c0bffa00f756ef3/8.png)

This screenshot show you how find your project folder. 
Right click on your project (this can be named different if you named it different when you create your project (see two steps backward)). Then the list of options will popup. Click on option that is in the picture.

#### This is now as readme file but in the future I'm planning to make it as wiki including some code snippets


## Thank you for using this repo and folow the steps that I gave you here. If something is not clear or missing, create issue.
